use log::{debug, warn};
use rdkafka::consumer::{ConsumerContext, Rebalance};
use rdkafka::error::KafkaResult;
use rdkafka::producer::ProducerContext;
use rdkafka::{ClientContext, Message, TopicPartitionList};

pub struct ConsumerCallbackLogger {}

impl ClientContext for ConsumerCallbackLogger {}

impl ConsumerContext for ConsumerCallbackLogger {
    fn pre_rebalance<'a>(&self, rebalance: &Rebalance<'a>) {
        match rebalance {
            Rebalance::Assign(topic_partition_list) => {
                for e in topic_partition_list.elements() {
                    debug!("rebalanced partition {}", e.partition())
                }
            }
            Rebalance::Revoke => debug!("all partitions are revoked"),
            Rebalance::Error(e) => warn!("post rebalance error {}", e),
        }
    }

    fn post_rebalance<'a>(&self, _rebalance: &Rebalance<'a>) {
        debug!("Preparing to rebalance");
    }

    fn commit_callback(&self, _result: KafkaResult<()>, offsets: &TopicPartitionList) {
        for e in offsets.elements() {
            if e.error().is_ok() {
                debug!(
                    "Offset {:?} set in partition {} for topic {}",
                    &e.offset().to_raw().unwrap_or(0),
                    &e.partition(),
                    &e.topic()
                );
            } else {
                warn!(
                    "Setting offset {:?} in partition {} for topic {} failed: {}",
                    &e.offset().to_raw().unwrap_or(0),
                    &e.partition(),
                    &e.topic(),
                    &e.error().unwrap_err()
                );
            }
        }
    }

    fn message_queue_nonempty_callback(&self) {
        debug!("New messages arriving...");
    }
}

pub struct ProducerCallbackLogger {}

impl ClientContext for ProducerCallbackLogger {}

impl ProducerContext for ProducerCallbackLogger {
    type DeliveryOpaque = ();

    fn delivery(
        &self,
        delivery_result: &rdkafka::producer::DeliveryResult<'_>,
        _delivery_opaque: Self::DeliveryOpaque,
    ) {
        let res = delivery_result.as_ref();
        match res {
            Ok(msg) => {
                let key: &str = msg.key_view().unwrap().unwrap();
                debug!(
                    "produced message with key {} in offset {} of partition {}",
                    key,
                    msg.offset(),
                    msg.partition()
                )
            }
            Err(e) => {
                let key: &str = e.1.key_view().unwrap().unwrap();
                warn!("failed to produce message with key {} - {}", key, e.0);
            }
        }
    }
}
