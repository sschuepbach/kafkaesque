//! Contains the shared models

use anyhow::{Context, Result};
use log::warn;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug)]
pub struct Locality {
    /// Name of locality
    pub name: String,
    /// Zip code of locality
    pub zip: u16,
    /// Additional code given by the FSO
    pub additional_digits: u16,
    /// Municipality to which the locality pertains to
    pub municipality: String,
    /// Number given by the FSO
    pub fso_number: u16,
    /// Canton in which the locality is located
    pub canton: String,
    /// Longitude of locality
    pub longitude: f64,
    /// Latitude of locality
    pub latitude: f64,
    /// Official spoken language
    pub language: String,
}

impl Locality {
    /// Creates JSON object from row in CSV file. Expects the string to be in the form of
    /// ```
    /// name;zip;additional_digits;municipality;fso_number;canton;longitude;latitude;language
    /// ```
    /// The elements should be parsable to the following types
    ///
    /// | Element           | Type     |
    /// |-------------------|----------|
    /// | name              | `String` |
    /// | zip               | `u16`    |
    /// | additional_digits | `u16`    |
    /// | municipality      | `String` |
    /// | fso_number        | `u16`    |
    /// | canton            | `String` |
    /// | longitude         | `f64`    |
    /// | latitude          | `f64`    |
    /// | language          | `String` |
    ///
    pub fn create_from_csv(raw_string: &str) -> Locality {
        let mut split = raw_string.split(';');
        let name = if let Some(n) = split.next() {
            n.to_owned()
        } else {
            warn!(
                "No locality name in CSV string found! Original string: {}",
                raw_string
            );
            String::new()
        };
        let zip: u16 = if let Some(z) = split.next() {
            z.parse().unwrap_or_else(|_| {
                warn!(
                    "Parsing of zip ({}) in CSV string failed! Original string: {}",
                    z, raw_string
                );
                0
            })
        } else {
            warn!("No zip in CSV string found!");
            0
        };
        let additional_digits: u16 = if let Some(ad) = split.next() {
            ad.parse().unwrap_or_else(|_| {
                warn!(
                    "Parsing of additional digits ({}) in CSV string failed! Original string: {}",
                    ad, raw_string
                );
                0
            })
        } else {
            warn!(
                "No additional digits in CSV string found! Original string: {}",
                raw_string
            );
            0
        };
        let municipality = if let Some(m) = split.next() {
            m.to_owned()
        } else {
            warn!(
                "No municipality in CSV string found! Original string: {}",
                raw_string
            );
            String::new()
        };
        let fso_number: u16 = if let Some(fsn) = split.next() {
            fsn.parse().unwrap_or_else(|_| {
                warn!(
                    "Parsing of additional digits ({}) in CSV string failed! Original string: {}",
                    fsn, raw_string
                );
                0
            })
        } else {
            warn!(
                "No FSO number in CSV string found! Original string: {}",
                raw_string
            );
            0
        };
        let canton = if let Some(c) = split.next() {
            c.to_owned()
        } else {
            warn!(
                "No canton in CSV string found! Original string: {}",
                raw_string
            );
            String::new()
        };
        let longitude: f64 = if let Some(long) = split.next() {
            long.parse().unwrap_or_else(|_| {
                warn!(
                    "Parsing of longitude ({}) in CSV string failed! Original string: {}",
                    long, raw_string
                );
                0.0
            })
        } else {
            warn!(
                "No longitude in CSV string found! Original string: {}",
                raw_string
            );
            0.0
        };
        let latitude: f64 = if let Some(lat) = split.next() {
            lat.parse().unwrap_or_else(|_| {
                warn!(
                    "Parsing of latitude ({}) in CSV string failed! Original string: {}",
                    lat, raw_string
                );
                0.0
            })
        } else {
            warn!(
                "No latitude in CSV string found! Original string: {}",
                raw_string
            );
            0.0
        };
        let language = if let Some(l) = split.next() {
            l.trim().to_owned()
        } else {
            warn!(
                "No language code in CSV string found! Original string: {}",
                raw_string
            );
            String::from("")
        };
        Locality {
            name,
            zip,
            additional_digits,
            municipality,
            fso_number,
            canton,
            longitude,
            latitude,
            language,
        }
    }

    /// Serialize struct as vector of bytes
    pub fn serialize(&self) -> Result<Vec<u8>> {
        let bytes = serde_json::to_vec(&self).context("Couldn't serialize object")?;
        Ok(bytes)
    }

    /// Create struct from vector of bytes
    pub fn deserialize(bytes: Vec<u8>) -> Result<Self> {
        let locality = serde_json::from_slice(bytes.as_slice())?;
        Ok(locality)
    }
}
