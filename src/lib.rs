use rdkafka::config::FromClientConfigAndContext;
use rdkafka::error::KafkaError;
use rdkafka::{ClientConfig, ClientContext};
use std::fmt::Display;

pub mod contexts;
pub mod models;

#[derive(Debug)]
pub struct ConfigError {
    msg: &'static str,
}

impl Display for ConfigError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.msg)
    }
}

impl std::error::Error for ConfigError {}

#[derive(Debug)]
pub enum Error {
    ConfigError(ConfigError),
    KafkaError(KafkaError),
}

impl From<ConfigError> for Error {
    fn from(err: ConfigError) -> Self {
        Error::ConfigError(err)
    }
}

impl From<KafkaError> for Error {
    fn from(err: KafkaError) -> Self {
        Error::KafkaError(err)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // TODO!
        write!(f, "")
    }
}

impl std::error::Error for Error {}

fn create_client_config(
    bootstrap_servers: &str,
    configurations: Vec<&str>,
) -> Result<ClientConfig, ConfigError> {
    let mut config = ClientConfig::new();
    config.set("bootstrap.servers", bootstrap_servers);
    for configuration in configurations {
        let mut key_value = configuration.split('=');
        if let Some(key) = key_value.next() {
            if let Some(value) = key_value.next() {
                config.set(key, value);
            } else {
                return Err(ConfigError {
                    msg: "No config value found",
                });
            }
        } else {
            return Err(ConfigError {
                msg: "No config key found",
            });
        }
    }
    Ok(config)
}

pub fn create_client<C, T>(
    bootstrap_servers: &str,
    configurations: Vec<&str>,
    ctx: C,
) -> Result<T, Error>
where
    C: ClientContext,
    T: FromClientConfigAndContext<C>,
{
    create_client_config(bootstrap_servers, configurations)?
        .create_with_context(ctx)
        .map_err(|e| e.into())
}
