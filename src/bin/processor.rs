use anyhow::{Context, Result};
use clap::{App, Arg, ArgMatches};
use kafka::contexts::{ConsumerCallbackLogger, ProducerCallbackLogger};
use kafka::models::Locality;
use log::{debug, error, warn};
use rdkafka::consumer::{BaseConsumer, CommitMode, Consumer};
use rdkafka::error::{KafkaError, RDKafkaErrorCode};
use rdkafka::producer::{BaseRecord, ThreadedProducer};
use rdkafka::Message;
use std::collections::HashMap;
use std::process;
use std::str;

fn parse_config() -> ArgMatches<'static> {
    App::new("Kafka count processor")
        .version("0.1.1")
        .author("Sebastian Schüpbach <post@annotat.net>")
        .about("Counts occurrences of certain fields in locality objects")
        .arg(
            Arg::with_name("bootstrap_servers")
                .short("b")
                .long("bootstrap-servers")
                .value_name("HOST:PORT[,HOST:PORT]")
                .help("Sets one or more Kafka bootstrap servers")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("input_topic")
                .short("i")
                .long("input-topic")
                .value_name("NAME")
                .help("Sets input topic name")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("output_topic")
                .short("o")
                .long("output-topic")
                .value_name("NAME")
                .help("Sets output topic name")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("consumer_configs")
                .short("c")
                .long("consumer-config")
                .value_name("key=value")
                .help("Defines a Kafka consumer configuration")
                .takes_value(true)
                .multiple(true)
                .number_of_values(1),
        )
        .arg(
            Arg::with_name("producer_configs")
                .short("p")
                .long("producer-config")
                .value_name("key=value")
                .help("Defines a Kafka producer configuration")
                .takes_value(true)
                .multiple(true)
                .number_of_values(1),
        )
        .get_matches()
}

struct Counter {
    locality: HashMap<String, usize>,
    municipality: HashMap<String, usize>,
    canton: HashMap<String, usize>,
    language: HashMap<String, usize>,
}

impl Counter {
    fn new() -> Self {
        Counter {
            locality: HashMap::new(),
            municipality: HashMap::new(),
            canton: HashMap::new(),
            language: HashMap::new(),
        }
    }

    fn add_locality(&mut self, name: &str) -> usize {
        if let Some(v) = self.locality.get_mut(name) {
            *v += 1;
            *v
        } else {
            self.locality.insert(name.to_owned(), 1);
            1
        }
    }

    fn add_municipality(&mut self, name: &str) -> usize {
        if let Some(v) = self.municipality.get_mut(name) {
            *v += 1;
            *v
        } else {
            self.municipality.insert(name.to_owned(), 1);
            1
        }
    }

    fn add_canton(&mut self, name: &str) -> usize {
        if let Some(v) = self.canton.get_mut(name) {
            *v += 1;
            *v
        } else {
            self.canton.insert(name.to_owned(), 1);
            1
        }
    }

    fn add_language(&mut self, name: &str) -> usize {
        if let Some(v) = self.language.get_mut(name) {
            *v += 1;
            *v
        } else {
            self.language.insert(name.to_owned(), 1);
            1
        }
    }
}

fn main() -> Result<()> {
    env_logger::init();

    let matches = parse_config();
    let bootstrap_servers = matches
        .value_of("bootstrap_servers")
        .context("bootstrap servers")?;
    let input_topic = matches.value_of("input_topic").context("input topic")?;
    let output_topic = matches.value_of("output_topic").context("output topic")?;
    let consumer_configs = matches
        .values_of("consumer_configs")
        .and_then(|v| Some(v.collect::<Vec<&str>>()))
        .unwrap_or(vec![]);
    let producer_configs = matches
        .values_of("producer_configs")
        .and_then(|v| Some(v.collect::<Vec<&str>>()))
        .unwrap_or(vec![]);

    let consumer: BaseConsumer<ConsumerCallbackLogger> = kafka::create_client(
        bootstrap_servers,
        consumer_configs,
        ConsumerCallbackLogger {},
    )
    .context("consumer built")?;
    let producer: ThreadedProducer<ProducerCallbackLogger> = kafka::create_client(
        bootstrap_servers,
        producer_configs,
        ProducerCallbackLogger {},
    )
    .context("producer built")?;

    consumer
        .subscribe(&[input_topic])
        .expect("topic subscription failed");

    let mut counter = Counter::new();
    loop {
        for msg_result in consumer.iter() {
            match msg_result {
                Ok(msg) => {
                    if let Some(k) = msg.key_view() {
                        let key: &str = k.unwrap();
                        msg.payload()
                            .and_then(|p| {
                                if let Ok(s) = Locality::deserialize(p.to_vec()) {
                                    Some(s)
                                } else {
                                    warn!("Couldn't parse payload of message with key {}", &key);
                                    None
                                }
                            })
                            .map(|l| {
                                let count = format!("{}", counter.add_locality(&l.name));
                                let base_record =
                                    BaseRecord::to(output_topic).key(&l.name).payload(&count);
                                if producer.send(base_record).is_ok() {
                                    debug!("Committing message");
                                    // Possible errors are handled in commit callback
                                    let _ = consumer.commit_message(&msg, CommitMode::Sync);
                                } else {
                                    warn!("Committing message failed!");
                                }
                                let count =
                                    format!("{}", counter.add_municipality(&l.municipality));
                                let base_record = BaseRecord::to(output_topic)
                                    .key(&l.municipality)
                                    .payload(&count);
                                if producer.send(base_record).is_ok() {
                                    debug!("Committing message");
                                    // Possible errors are handled in commit callback
                                    let _ = consumer.commit_message(&msg, CommitMode::Sync);
                                } else {
                                    warn!("Committing message failed!");
                                }
                                let count = format!("{}", counter.add_canton(&l.canton));
                                let base_record =
                                    BaseRecord::to(output_topic).key(&l.canton).payload(&count);
                                if producer.send(base_record).is_ok() {
                                    debug!("Committing message");
                                    // Possible errors are handled in commit callback
                                    let _ = consumer.commit_message(&msg, CommitMode::Sync);
                                } else {
                                    warn!("Committing message failed!");
                                }
                                let count = format!("{}", counter.add_language(&l.language));
                                let base_record = BaseRecord::to(output_topic)
                                    .key(&l.language)
                                    .payload(&count);
                                if producer.send(base_record).is_ok() {
                                    debug!("Committing message");
                                    // Possible errors are handled in commit callback
                                    let _ = consumer.commit_message(&msg, CommitMode::Sync);
                                } else {
                                    warn!("Committing message failed!");
                                }
                            });
                    } else {
                        warn!("Message with no key!")
                    }
                }
                Err(e) => match e {
                    KafkaError::MessageConsumption(e2) => match e2 {
                        RDKafkaErrorCode::UnknownTopicOrPartition => {
                            error!("Topic '{}' is unknown", &input_topic);
                            process::exit(1);
                        }
                        _ => warn!("{}", e2),
                    },
                    _ => warn!("{}", e),
                },
            }
        }
    }
}
