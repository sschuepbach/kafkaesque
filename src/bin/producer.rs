use anyhow::{Context, Result};
use clap::{App, Arg, ArgMatches};
use kafka::contexts::ProducerCallbackLogger;
use kafka::models::Locality;
use log::warn;
use rdkafka::producer::{BaseRecord, ThreadedProducer};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::str;

fn parse_config() -> ArgMatches<'static> {
    App::new("Kafka producer")
        .version("0.1.1")
        .author("Sebastian Schüpbach <post@annotat.net>")
        .about("Reads content from a file and writes it to Kafka topic")
        .arg(
            Arg::with_name("bootstrap_servers")
                .short("b")
                .long("bootstrap-servers")
                .value_name("HOST:PORT[,HOST:PORT]")
                .help("Sets one or more Kafka bootstrap servers")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("output_topic")
                .short("o")
                .long("output-topic")
                .value_name("NAME")
                .help("Sets output topic name")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("producer_configs")
                .short("p")
                .long("producer-config")
                .value_name("key=value")
                .help("Defines a Kafka producer configuration")
                .takes_value(true)
                .multiple(true)
                .number_of_values(1),
        )
        .arg(
            Arg::with_name("repeat_ingest")
                .short("r")
                .long("repeat")
                .value_name("NUM")
                .help("Defines how much the content of the file is written to topic")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("INPUT")
                .help("Sets the input file to use")
                .required(true)
                .index(1),
        )
        .get_matches()
}

fn main() -> Result<()> {
    env_logger::init();
    let matches = parse_config();
    let bootstrap_servers = matches.value_of("bootstrap_servers").unwrap();
    let output_topic = matches.value_of("output_topic").unwrap();
    let repeat_ingest: usize = matches
        .value_of("repeat_ingest")
        .and_then(|r| r.parse().ok())
        .unwrap_or(1);
    let input_file = matches.value_of("INPUT").unwrap();
    let producer_configs = matches
        .values_of("producer_configs")
        .and_then(|v| Some(v.collect::<Vec<&str>>()))
        .unwrap_or(vec![]);

    let producer: ThreadedProducer<ProducerCallbackLogger> = kafka::create_client(
        bootstrap_servers,
        producer_configs,
        ProducerCallbackLogger {},
    )
    .context("producer built")?;

    let file = File::open(&input_file)?;
    let file = BufReader::new(file);
    for line in file.lines() {
        let locality = Locality::create_from_csv(&line.unwrap());
        locality
            .serialize()
            .map(|s| {
                for _ in 0..repeat_ingest {
                    let base_record = BaseRecord::to(output_topic).key(&locality.name).payload(&s);
                    let r = producer.send(base_record);
                    if r.is_err() {
                        warn!("Committing message for locality {} failed", &locality.name);
                    }
                }
            })
            .unwrap_or_else(|e| {
                warn!(
                    "Couldn't serialize payload of message with locality {}: {}",
                    &locality.name, &e
                );
            });
    }
    Ok(())
}
