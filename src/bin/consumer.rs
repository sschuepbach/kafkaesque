use anyhow::{Context, Result};
use clap::{App, Arg, ArgMatches};
use kafka::contexts::ConsumerCallbackLogger;
use log::{error, warn};
use rdkafka::consumer::{BaseConsumer, Consumer};
use rdkafka::error::{KafkaError, RDKafkaErrorCode};
use rdkafka::Message;
use std::process;
use std::str;

fn parse_config() -> ArgMatches<'static> {
    App::new("Kafka consumer")
        .version("0.1.1")
        .author("Sebastian Schüpbach <post@annotat.net>")
        .about("Consumes Kafka topic and prints out key and value")
        .arg(
            Arg::with_name("bootstrap_servers")
                .short("b")
                .long("bootstrap-servers")
                .value_name("HOST:PORT[,HOST:PORT]")
                .help("Sets one or more Kafka bootstrap servers")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("input_topic")
                .short("i")
                .long("input-topic")
                .value_name("NAME")
                .help("Sets input topic name")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("consumer_configs")
                .short("c")
                .long("consumer-config")
                .value_name("key=value")
                .help("Defines a Kafka consumer configuration")
                .takes_value(true)
                .multiple(true)
                .number_of_values(1),
        )
        .get_matches()
}

fn main() -> Result<()> {
    env_logger::init();

    let matches = parse_config();
    let bootstrap_servers = matches.value_of("bootstrap_servers").unwrap();
    let input_topic = matches.value_of("input_topic").unwrap();
    let consumer_configs = matches
        .values_of("consumer_configs")
        .and_then(|v| Some(v.collect::<Vec<&str>>()))
        .unwrap_or(vec![]);

    let consumer: BaseConsumer<ConsumerCallbackLogger> = kafka::create_client(
        bootstrap_servers,
        consumer_configs,
        ConsumerCallbackLogger {},
    )
    .context("consumer built")?;

    consumer
        .subscribe(&[input_topic])
        .expect("topic subscription failed");

    loop {
        for msg_result in consumer.iter() {
            match msg_result {
                Ok(msg) => {
                    if let Some(k) = msg.key_view() {
                        let key: &str = k.unwrap();
                        msg.payload()
                            .and_then(|p| {
                                if let Ok(s) = str::from_utf8(p) {
                                    Some(s)
                                } else {
                                    warn!("Couldn't parse payload of message with key {}", &key);
                                    None
                                }
                            })
                            .map(|s| {
                                println!("{} - {}", key, s);
                            });
                    } else {
                        warn!("Message with no key!")
                    }
                }
                Err(e) => match e {
                    KafkaError::MessageConsumption(e2) => match e2 {
                        RDKafkaErrorCode::UnknownTopicOrPartition => {
                            error!("Topic '{}' is unknown", &input_topic);
                            process::exit(1);
                        }
                        _ => warn!("{}", e2),
                    },
                    _ => warn!("{}", e),
                },
            }
        }
    }
}
