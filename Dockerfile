FROM index.docker.io/library/rust:alpine AS build
ARG APPNAME=kafkaesque

RUN apk update && apk add musl-dev upx build-base cmake openssl-dev

WORKDIR /buildenv
RUN USER=root cargo new -q --vcs none --bin $APPNAME
WORKDIR /buildenv/$APPNAME
COPY Cargo.toml .
COPY Cargo.lock .
RUN cargo build --release

RUN rm -r src && rm -f ./target/release/deps/$APPNAME*
COPY src/ src/
RUN cargo build --release && strip ./target/release/$APPNAME && upx --best -q --lzma ./target/release/$APPNAME
RUN mkdir /build && mv ./target/release/$APPNAME /build/app

FROM scratch
WORKDIR /app
COPY --from=build /build/app app
ENTRYPOINT ["./app", "./config/config.toml"]
