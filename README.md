# Kafkaesque

__Highly__ experimental Kafka workflow implemented in Rust.

At the moment, the workflow consists of three services:

* A [producer](https://sschuepbach.pages.switch.ch/kafkaesque/producer) which
  reads data from a CSV file, parse its rows and writes them into a Kafka
  topic
* an intermediary
  [processor](https://sschuepbach.pages.switch.ch/kafkaesque/processor)
  counting values in certain fields of the incoming data and
* a final
  [consumer](https://sschuepbach.pages.switch.ch/kafkaesque/consumer), which
  simply prints the key and a respective value (i.e. the count)
  
## Build

1. Install the standard Rust tools via [Rustup](https://rustup.rs)
2. Clone the repository and `cd` into its root directory
3. Run `cargo build --release`

You should now have three executable binaries inside `target/release`:
* `producer`
* `processor`
* `consumer`

For further information see the respective documentations or the command-line
help by typing `<bin> --help`.


## Run on minikube with local Kafka cluster

### Connecting to Kafka cluster on host system

In order to communicate with a local Kafka cluster, a "hack" has to be applied
to the Kafka settings (for further explanations see
[here](https://www.confluent.de/blog/kafka-client-cannot-connect-to-broker-on-aws-on-docker-etc/),
especially _Scenario 5: Kafka running locally with a client in Docker
container_):

```
listeners=PLAINTEXT://:9092,MINIKUBE://:19092
advertised.listeners=PLAINTEXT://localhost:9092,MINIKUBE://host.minikube.internal:19092
listener.security.protocol.map=PLAINTEXT:PLAINTEXT,MINIKUBE:PLAINTEXT
```

This essentially adds a new listener to Kafka (`MINIKUBE`), which is used for the
connections originating inside Minikube, while keeping the original listener
(`PLAINTEXT`) intact for connections originating directly from the host
system. The important part here is the new address of the
`advertised.listeners`, `MINIKUBE://host.minikube.internal:19092`. If
`localhost:9092` was advertised to a application running inside Minikube, the
connection to a broker would fail, since _inside_ the respective container
nothing listens on `localhost:9092`. On the other hand,
`host.minikube.internal` is [used by Minikube](https://minikube.sigs.k8s.io/docs/handbook/host-access/) to solve
the problem of the dynamic IP address of the host system seen from inside
Minikube. Using a recent version of Minikube, `host.minikube.internal` should
always resolve to the correct host IP address.

### Making input data accessible

After Minikube has been started, you have to mount the example data into
Minikube in order to make it accessible to the producer application:

```
minikube mount <host_dir>:<minikube_mount>
```

### Using the Kubernetes manifests

There are some basic manifests in the `k8s` directory. You can use them as a
starting point, but consider changing the `args` section according to your
needs.
